
public class MapPoint {
	public int positionX;
	public int positionY;
	public boolean isLeftBlocked;
	public boolean isTopBlocked;
	public boolean isRightBlocked;
	public boolean isBottomBlocked;
	public boolean hasColor;
}
