import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

import java.io.*;
import lejos.pc.comm.*;

public class Monitor extends JFrame
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8107065549655191705L;
	private static final int BOX_SIZE = 30;
	static InputStream is;
	static DataInputStream str;
			
	public Robot robot;
	public Map currentMap;
	public Map otherFloorMap;
	
	Monitor()
	{
		super("Map - Lab #10");
		setSize( 800, 800 );
		setVisible( true );
		currentMap = new Map();
		otherFloorMap = new Map();
	}
	
	public static void main(String[] args) throws Exception
	{	
		Monitor monitor = new Monitor();
		
		monitor.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        
        NXTConnector conn = new NXTConnector();

        // Connect to any NXT over Bluetooth
        boolean connected = conn.connectTo("btspp://MunRobo" );

        if ( connected ) 
        {
            System.out.println("Connected!");
        }
        else
        {
            System.out.println("Could not connect to any NXTs");
            System.exit(1);
        }

        is = conn.getInputStream();
		str = new DataInputStream( is );
		
		while( true )
		{
			char type = str.readChar();

			switch (type) {
				case 's':
					int posX = str.readInt();
					int posY = str.readInt();
					boolean isLeftBlocked = str.readBoolean();
					boolean isTopBlocked = str.readBoolean();
					boolean isRightBlocked = str.readBoolean();
					boolean isBottomBlocked = str.readBoolean();
					boolean isColoredPoint = str.readBoolean();

					monitor.currentMap.update(posX, posY, isLeftBlocked, isTopBlocked, isRightBlocked, isBottomBlocked, isColoredPoint);
					
					System.out.println("Robot position: " + posX + "," + posY);
					System.out.println("Left: " + isLeftBlocked + ", Top: " + isTopBlocked + ", Right: " + isRightBlocked + ", Bottom: " + isBottomBlocked);
					System.out.println("Has color: " + isColoredPoint);
					
					break;
				case 'f':
					Map tmp = monitor.currentMap;
					monitor.currentMap = monitor.otherFloorMap;
					monitor.otherFloorMap = tmp;
					
					break;
				default:
					break;
			}
			
			monitor.repaint();
		}
	}

	public void paint( Graphics g )
	{
		super.paint( g );
		displayMap(currentMap, g);
	}
	
	public void displayMap(Map m, Graphics g) {
		Graphics2D g2 = ( Graphics2D ) g;
		
		for (MapPoint p : m.getPoints()) {
			g2.setPaint( p.hasColor ? Color.black : Color.white );
			
			int centerX = 400 + p.positionX*BOX_SIZE;
			int centerY = 400 - p.positionY*BOX_SIZE;
			g2.draw( new Ellipse2D.Double(centerX, centerY, 1, 1) );
			
			// Top
			g2.setPaint( p.isTopBlocked ? Color.RED : Color.GREEN );
			g2.drawLine(centerX-(BOX_SIZE/2), centerY-(BOX_SIZE/2), centerX+(BOX_SIZE/2), centerY-(BOX_SIZE/2));
			
			// Right
			g2.setPaint( p.isRightBlocked ? Color.RED : Color.GREEN );
			g2.drawLine(centerX+(BOX_SIZE/2), centerY-(BOX_SIZE/2), centerX+(BOX_SIZE/2), centerY+(BOX_SIZE/2));
			
			// Bottom
			g2.setPaint( p.isBottomBlocked ? Color.RED : Color.GREEN );
			g2.drawLine(centerX-(BOX_SIZE/2), centerY+(BOX_SIZE/2), centerX+(BOX_SIZE/2), centerY+(BOX_SIZE/2));
			
			// Left
			g2.setPaint( p.isLeftBlocked ? Color.RED : Color.GREEN );
			g2.drawLine(centerX-(BOX_SIZE/2), centerY-(BOX_SIZE/2), centerX-(BOX_SIZE/2), centerY+(BOX_SIZE/2));
		}
		
		// Draw current location
		MapPoint target = m.getLastPoint();
		
		g2.setPaint( Color.red );
		g2.drawRect(400 + target.positionX*BOX_SIZE - 1, 400 - target.positionY*BOX_SIZE - 1, 2, 2);
	}
}
