import java.util.ArrayList;

public class Map {
	private ArrayList<MapPoint> points;
	private int lastX = 0, lastY = 0;
	
	public Map() {
		points = new ArrayList<MapPoint>();
	}
	
	public ArrayList<MapPoint> getPoints() {
		return points;
	}
	
	public MapPoint getLastPoint() {
		MapPoint t = new MapPoint();
		t.positionX = lastX;
		t.positionY = lastY;
		
		return t;
	}
	
	public void update(int positionX, int positionY, boolean isLeftBlocked, boolean isTopBlocked, boolean isRightBlocked, boolean isBottomBlocked, boolean isColoredPoint) {
		lastX = positionX;
		lastY = positionY;
		
		for (MapPoint p : points) {
			if (p.positionX == positionX && p.positionY == positionY) {
				p.isLeftBlocked = isLeftBlocked;
				p.isTopBlocked = isTopBlocked;
				p.isRightBlocked = isRightBlocked;
				p.isBottomBlocked = isBottomBlocked;
				p.hasColor = isColoredPoint;
				
				return;
			}
		}
		
		MapPoint newPoint = new MapPoint();
		newPoint.positionX = positionX;
		newPoint.positionY = positionY;
		newPoint.isLeftBlocked = isLeftBlocked;
		newPoint.isTopBlocked = isTopBlocked;
		newPoint.isRightBlocked = isRightBlocked;
		newPoint.isBottomBlocked = isBottomBlocked;
		newPoint.hasColor = isColoredPoint;
		
		points.add(newPoint);
	}
}
